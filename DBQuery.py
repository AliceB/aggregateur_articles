import connexion_db as cd
import psycopg2.extras

def get_ids (url_flux):
    sql = """SELECT id_s FROM t_sources WHERE url_flux = %s"""
    with cd.conn_db() as conn:
        cur = conn.cursor()
        cur.execute(sql, (url_flux,))
        id_s = cur.fetchone()
        if id_s is None:
            return None
        else:
            return id_s[0]


def get_all_articles(title_search, id_source):
    
    q = """
        SELECT titre, date_publi, url_article, nm_source 
        FROM t_articles 
        INNER JOIN t_sources
        ON id_s = id_source
        """
    if title_search:
        q += """
                WHERE LOWER(titre) LIKE LOWER(%(title_search)s)
            """
    if id_source:
        q += """
                AND id_source IN %(id_source)s
            """
    q += """
            ORDER BY date_publi DESC
        """

    p = {
        "title_search": "%" + title_search + "%",
        "id_source" : id_source,
        }

    with cd.conn_db() as conn:
        cur = conn.cursor()
        cur.execute(q, p)
        retour = cur.fetchall()
        articles = []
        for elt in retour:
            art = {}
            art['titre'] = elt[0]
            art['date_publi'] = elt[1]
            art['url_article'] = elt[2]
            art['nm_source'] = elt[3]
            articles.append(art)

        return articles


def get_all_sources():
    q = """
        SELECT id_s, nm_source, url_flux 
        FROM t_sources 
        """
    with cd.conn_db() as conn:
        cur = conn.cursor()
        cur.execute(q)
        retour = cur.fetchall()
        sources = []
        for elt in retour:
            src = {}
            src['id_s'] = elt[0]
            src['nm_source'] = elt[1]
            src['url_flux'] = elt[2]
            sources.append(src)
        return sources


def get_user_sources(id_u):
    req = '''
            SELECT s.id_s, nm_source, url_flux 
            FROM t_utilisateurs_sources us
            INNER JOIN t_sources s
            ON us.id_s = s.id_s
            WHERE id_u = %s;
        '''
    with cd.conn_db() as conn:
        cur = conn.cursor()
        cur.execute(req,(id_u,))
        retour = cur.fetchall()
        sources = []
        for elt in retour:
            src = {}
            src['id_s'] = elt[0]
            src['nm_source'] = elt[1]
            src['url_flux'] = elt[2]
            sources.append(src)
        return sources


def get_sources_dispo(id_u):
    req = '''
            SELECT s.id_s, nm_source, url_flux 
            FROM t_sources s
            EXCEPT
            SELECT s.id_s, nm_source, url_flux 
            FROM t_utilisateurs_sources us
            INNER JOIN t_sources s
            ON us.id_s = s.id_s
            WHERE id_u = %s;
        '''

    with cd.conn_db() as conn:
        cur = conn.cursor()
        cur.execute(req,(id_u,))
        retour = cur.fetchall()
        sources_dispo = []
        for elt in retour:
            src = {}
            src['id_s'] = elt[0]
            src['nm_source'] = elt[1]
            src['url_flux'] = elt[2]
            sources_dispo.append(src)
        return sources_dispo


def get_user(nm_user,mail_user):
    '''teste si cet utilisateur et ce mail existe
    return : True si oui
    '''
    rq='''
    select id_u,log_u,mdp_u,email_u,statut_u 
    FROM t_utilisateurs
    WHERE log_u = %s OR email_u = %s;
    '''
    with cd.conn_db() as conn:
        cur = conn.cursor()
        cur.execute(rq,(nm_user,mail_user))
        res = cur.fetchall()
    user_exist= True if len(res)==1 else False
    return user_exist


def get_user_info(nm_user,mail_user):
    '''teste si cet utilisateur et ce mail existe
    return : True si oui
    '''
    rq='''
    select id_u,log_u,mdp_u,email_u,statut_u 
    FROM t_utilisateurs
    WHERE log_u = %s OR email_u = %s;
    '''
    with cd.conn_db() as conn:
        cur = conn.cursor()
        cur.execute(rq,(nm_user,mail_user))
        res = cur.fetchall()
        id_u = res[0][0]
        log_u = res[0][1]
        mdp_u = res[0][2]
        email_u = res[0][3]
        statut_u = res[0][4]
    
    return id_u, log_u, mdp_u, email_u, statut_u


def get_user_mdp(nm_user,mail_user):
    '''teste si cet utilisateur ou ce mail existe
    return : True si oui
    '''
    
    rq='''
    select id_u,log_u,mdp_u,email_u,statut_u 
    FROM t_utilisateurs
    WHERE (log_u = %s OR email_u=%s);
    '''
    with cd.conn_db() as conn:
        cur = conn.cursor()
        cur.execute(rq,(nm_user,mail_user))
        res = cur.fetchall()
    user_exist= True if len(res)==1 else False
    return user_exist

if __name__ == "__main__":
    # test = get_all_sources()
    # print(test)
    # print(test[0]["id_s"])
    # print(test[0]["nm_source"])
    # print(test[0]["url_flux"])
    # test = get_ids("https://www.lemonde.fr/pixels/rss_full.xml")
    # print(test)
    # test1 = get_ids("toto")
    # print(test1)
    # test = get_sources_dispo(1)
    # print(test)
    # test = get_user("Victor", "yo3@yo.com")
    # print(test)
    sources_fav = get_user_sources(5)
    print(sources_fav)



    
