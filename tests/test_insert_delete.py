import sys
import pytest

sys.path.append("..")

import connexion_db as cd
import insert_delete as idel
import psycopg2 as p2
from faker import Faker
from faker.providers import lorem, internet, date_time


@pytest.fixture(scope="session")
def conn():
    with cd.conn_db() as conn:
        yield conn


@pytest.fixture(scope="session")
def faker():

    faker = Faker()
    faker.add_provider(lorem)
    faker.add_provider(internet)
    faker.add_provider(date_time)
    
    return faker


@pytest.fixture(scope="session")
def sources(faker):

    sources = []
    sources.append({
                'nm_source': faker.sentence(nb_words=5),
                'url_source': faker.url(),
                'url_flux': faker.uri(),
                'type_flux': faker.lexify(text='????')
            })

    sources.append({
                'nm_source': faker.sentence(nb_words=5),
                'url_source': faker.url(),
                'url_flux': faker.uri(),
                'type_flux': faker.lexify(text='????')
            })

    return sources


def test_insertion_sources(conn,sources):
    cur = conn.cursor()

    cur.execute('SELECT COALESCE(MAX(id_s),0) FROM t_sources;')
    max_id = cur.fetchone()[0]

    sql = 'ALTER SEQUENCE t_sources_id_s_seq RESTART WITH %s'
    cur.execute(sql,(max_id+1,))
    
    for src in sources:
        test_retour = idel.insert_source(conn,src)
        # L'id retourné est bien le max précédent plus le nombre de sources insérées
        assert test_retour == max_id + 1
        src['id_source'] = test_retour

        #les lignes sont bien dedans
        cur.execute('SELECT COALESCE(MAX(id_s),0) FROM t_sources;')
        new_max_id = cur.fetchone()[0]

        assert new_max_id == max_id + 1

        max_id += 1


def test_insertion_articles(conn, sources,faker):
    cur = conn.cursor()

    cur.execute('SELECT COALESCE(MAX(id_a),0) FROM t_articles;')
    max_id = cur.fetchone()[0]

    sql = 'ALTER SEQUENCE t_articles_id_a_seq RESTART WITH %s'
    cur.execute(sql,(max_id+1,))

    for src in sources:
        articles = []
        articles.append({
            'id_source': src['id_source'],
            'titre': faker.sentence(nb_words=8),
            'date_publi': faker.iso8601(tzinfo=None, end_datetime=None),
            'description': faker.paragraph(nb_sentences=3, variable_nb_sentences=True),
            'url_article': faker.uri(),
            'date_entree': '2020-03-09'
        })
        articles.append({
            'id_source': src['id_source'],
            'titre': faker.sentence(nb_words=8),
            'date_publi': faker.iso8601(tzinfo=None, end_datetime=None),
            'description': faker.paragraph(nb_sentences=3, variable_nb_sentences=True),
            'url_article': faker.uri(),
            'date_entree': '2020-03-09'
        })

        idel.insert_article(conn,articles)

        cur.execute('SELECT COALESCE(MAX(id_a),0) FROM t_articles;')
        new_max_id = cur.fetchone()[0]

        assert new_max_id == max_id + 2
        max_id = new_max_id


def test_unicite_article(conn,sources,faker):
    cur = conn.cursor()
    
    sql = 'SELECT COUNT(*) FROM t_articles;'
    cur.execute(sql)
    nbavant = cur.fetchone()[0]

    article = [{
            'id_source': sources[0]['id_source'],
            'titre': faker.sentence(nb_words=8),
            'date_publi': faker.iso8601(tzinfo=None, end_datetime=None),
            'description': faker.paragraph(nb_sentences=3, variable_nb_sentences=True),
            'url_article': faker.uri(),
            'date_entree': '2020-03-09'
        }]

    idel.insert_article(conn,article)
    idel.insert_article(conn,article)

    cur.execute(sql)
    nbapres = cur.fetchone()[0]

    assert nbapres == nbavant + 1


def test_suppression_sources(conn,sources):
    cur = conn.cursor()
    req = 'SELECT * FROM t_sources WHERE url_flux = %s'
    for src in sources:
        idel.remove_source(conn,src["url_flux"])
        cur.execute(req,(src['url_flux'],))
        assert len(cur.fetchall()) == 0
