import connexion_db

#fonction d'insertion d'articles dan la table t_articles
def insert_article(conn, dict_list):
    
    sql = """INSERT INTO t_articles (
                titre,
                date_publi,
                description,
                url_article, 
                date_entree, 
                id_source 
                )
            VALUES (
                %(titre)s,
                %(date_publi)s,
                %(description)s,
                %(url_article)s,
                %(date_entree)s,
                %(id_source)s)
                ON CONFLICT (titre) DO NOTHING;"""

    for art in dict_list:
        cur = conn.cursor()
        cur.execute(sql, art)
        conn.commit()
    print(f"{len(dict_list)} articles insérés")


#fonction d'insertion de sources dans la table t_sources
def insert_source(conn, dict_row):
    
    sql = """INSERT INTO t_sources (
                nm_source,
                url_source,
                url_flux,
                type_flux
                )
            VALUES (
                %(nm_source)s,
                %(url_source)s,
                %(url_flux)s,
                %(type_flux)s)
            ON CONFLICT (url_flux) DO NOTHING RETURNING id_s;"""


    cur = conn.cursor()
    cur.execute(sql, dict_row)
    last_id = cur.fetchone()
    conn.commit()
    if last_id is not None:
        return last_id[0]


#fonction de suppression de source de la table t_sources
def remove_source (conn, id_s):

    #avant de supprimer une source, on supprime tous les articles correspondant des t_utilisateurs_sources et t_articles
    remove_source_tous_utilisateurs(conn, id_s)
    remove_tous_articles_source (conn, id_s)
    #puis on supprime la ligne de la table t_sources
    sql = """DELETE FROM t_sources WHERE id_s = %s;"""
    cur = conn.cursor()
    cur.execute(sql, (id_s,))
    conn.commit()
    print(f"source supprimée: {id_s}")


#fonction qui supprime tous les articles d'une source de la table t_articles
def remove_tous_articles_source (conn, id_source):

    #on récupere l' Id de la source dont on veut supprimer les articles
    # select_ids = """SELECT id_s FROM t_sources WHERE url_flux = %s;"""
    # cur = conn.cursor()
    # cur.execute(select_ids, (url_flux,))
    # id_s = cur.fetchone()
    #puis on supprime tous les articles correspondants de la table t_articles
    sql = """DELETE FROM t_articles WHERE id_source = %s;"""
    cur = conn.cursor()
    cur.execute(sql, (id_source,))
    conn.commit()
    print(f"articles supprimés de la source {id_source}")


def insert_utilisateur(conn, dict_utilisateur):
    sql = """INSERT INTO t_utilisateurs (
                log_u,
                mdp_u,
                email_u,
                statut_u
                )
            VALUES (
                %(log_u)s,
                %(mdp_u)s,
                %(email_u)s,
                %(statut_u)s
                );"""

    cur = conn.cursor()
    cur.execute(sql, dict_utilisateur)
    conn.commit()


def add_source_utilisateur(conn, id_u, id_s):
    
    dict_us = {"id_u" : id_u,
                "id_s" : id_s,
                }

    sql = """INSERT INTO t_utilisateurs_sources (
                id_u,
                id_s
                )
            VALUES (
                %(id_u)s,
                %(id_s)s
                );"""
    
    cur = conn.cursor()
    cur.execute(sql, dict_us)
    conn.commit()


def remove_source_utilisateur(conn, id_u, id_s):
    #on récupere les id_a des articles associés à la source qu'on veut supprimer
    select_id_a = """SELECT id_a FROM t_articles WHERE id_source = %s;"""
    cur = conn.cursor()
    cur.execute(select_id_a, (id_s,))
    articles_a_supprimer = cur.fetchall()
    #on supprime tous les articles correspondants de la table t_utilisateurs_articles
    for art in articles_a_supprimer:
        sql = """DELETE FROM t_utilisateurs_articles WHERE id_a = %s AND id_u = %s;"""
        cur = conn.cursor()
        cur.execute(sql, (art, id_u))
        conn.commit()
    #on supprime la source pour l'utilisateur en question
    sql = """DELETE FROM t_utilisateurs_sources WHERE id_s = %s AND id_u = %s;"""
    cur = conn.cursor()
    cur.execute(sql, (id_s, id_u))
    conn.commit()


def remove_source_tous_utilisateurs(conn, id_s):
    #on récupere les id_a des articles associés à la source qu'on veut supprimer
    select_id_a = """SELECT id_a FROM t_articles WHERE id_source = %s;"""
    cur = conn.cursor()
    cur.execute(select_id_a, (id_s,))
    articles_a_supprimer = cur.fetchall()
    #on supprime tous les articles correspondants de la table t_utilisateurs_articles
    for art in articles_a_supprimer:
        sql = """DELETE FROM t_utilisateurs_articles WHERE id_a = %s;"""
        cur = conn.cursor()
        cur.execute(sql, (art,))
        conn.commit()
    #on supprime la source pour tous les utilisateurs
    sql = """DELETE FROM t_utilisateurs_sources WHERE id_s = %s;"""
    cur = conn.cursor()
    cur.execute(sql, (id_s,))
    conn.commit()


def insert_article_utilisateur(conn, id_u, id_a, lu = True):
    dict_art = {"id_u" : id_u,
                "id_a" : id_a,
                "lu" : lu,
                }

    sql = """INSERT INTO t_utilisateurs_articles (
                id_u,
                id_a,
                lu
                )
            VALUES (
                %(id_u)s,
                %(id_s)s,
                %(lu)s
                );"""

    cur = conn.cursor()
    cur.execute(sql, dict_art)
    conn.commit()


def article_lu(conn, id_u, id_a, lu = True):
    dict_art = {"id_u" : id_u,
                "id_a" : id_a,
                "lu" : lu,
                }

    sql = """UPDATE t_utilisateurs_articles 
            SET lu = %(lu)s 
            WHERE id_u = %(id_u)s and id_a = %(id_a)s 
                ;"""

    cur = conn.cursor()
    cur.execute(sql, dict_art)
    conn.commit()


if __name__ == "__main__":
    #test avec une liste de sources et articles bidons
    test_list_src = {
                    "nm_source" : "yo",
                    "url_source" : ".com",
                    "url_flux" : "c",
                    "type_flux" : "rss",
                    }
                    
    
    test_list_art = [
                        {
                        "titre" : "2",
                        "date_publi" : "01/01/01",
                        "description" : "4",
                        "url_article" : "5", 
                        "date_entree" : "01/01/01", 
                        "id_source" : "25",
                        }
                    ]

    conn = connexion_db.conn_db()
    # remove_source(conn, 9)
    # id = insert_source(conn, test_list_src)
    # print(id)
    # remove_source(conn, "a")
    # insert_source(conn, test_list_src)
    # insert_article(conn, test_list_art)
    
    #ajout d'un user test
    user_test = {"log_u" : "qqun" , "mdp_u" : "hello", "email_u" : "yo@yo.com", "statut_u" : "admin"}
    user_test2 = {"log_u" : "qqun2" , "mdp_u" : "hello2", "email_u" : "yo2@yo.com", "statut_u" : "user"}
    insert_utilisateur(conn, user_test2)

