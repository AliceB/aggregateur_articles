import psycopg2
import psycopg2.extras

'''
Host
    ec2-54-247-79-178.eu-west-1.compute.amazonaws.com
Database
    d6vg3cijv005k4
User
    itnpczyqgxdiuk
Port
    5432
Password
    d3c468bda05a360c7764e821d2d41d3cd81c57f1d13de73eda335cf4150e48ea
URI
    postgres://itnpczyqgxdiuk:d3c468bda05a360c7764e821d2d41d3cd81c57f1d13de73eda335cf4150e48ea@ec2-54-247-79-178.eu-west-1.compute.amazonaws.com:5432/d6vg3cijv005k4
Heroku CLI
    heroku pg:psql postgresql-fluffy-63624 --app aggregateur-articles
'''


def conn_db():
    try:
        conn = psycopg2.connect(user = "aggreg",
                                    password = "art",
                                    host = "localhost",
                                    port = "5432",
                                    database = "aggreg_art")

        # conn = psycopg2.connect(user = "itnpczyqgxdiuk",
        #                             password = "d3c468bda05a360c7764e821d2d41d3cd81c57f1d13de73eda335cf4150e48ea",
        #                             host = "ec2-54-247-79-178.eu-west-1.compute.amazonaws.com",
        #                             port = "5432",
        #                             database = "d6vg3cijv005k4")

        print (conn.get_dsn_parameters())

        return conn

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)
    

def conn_db_close(conn):
    if(conn):
        conn.close()
        print("PostgreSQL connection is closed")

if __name__ == "__main__":
    conn = conn_db()
    conn_db_close(conn)
