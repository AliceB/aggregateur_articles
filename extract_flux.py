import feedparser, re
from datetime import datetime, date
from time import mktime

def extract_source(url_flux):
    """ fonction d'extraction des informations <feed> de la source
        parametre: url_flux = le flux RSS/ATOM à integrer dans l'aggrégateur
        return   : dico_source = un dictionnaire des données relatives a la source
    """
    
    fd = feedparser.parse(url_flux)
    #print(list(fd.feed.keys()))
    source_valide=False
    if list(fd.feed.keys())!=[]:
        dico_source = {'nm_source':'', 'url_source':'','type_flux':'', 'url_flux': url_flux}
        if 'title' in fd.feed:
            dico_source['nm_source']= fd.feed.title
            source_valide=True
        if 'link' in fd.feed:
            dico_source['url_source']= fd.feed.link
        if 'links' in fd.feed:
            for i, elt in enumerate(fd.feed.links):
                if fd.feed.links[i].type == 'application/rss+xml' or fd.feed.links[i].type == 'application/atom+xml':
                    dico_source['type_flux'] = fd.feed.links[i].type.replace('application/','')
    if source_valide == True:
        return dico_source

def extract_articles(url_flux,id_db_source=None):
    """ fonction d'extraction des informations <entries> des articles
        parametres: url_flux = le flux RSS/ATOM à integrer dans l'aggrégateur
                    id_db_source = le n° d'identifiant <id_s> de la source tel 
                                   que enregistré dans la table des <sources>
        return    : list_dicos_article = une liste de dictionnaire des données relatives
                                         aux articles de la source
    """
    fd = feedparser.parse(url_flux)
    if list(fd.feed.keys())!=[]:
        list_dicos_article = []
        for i, elem in enumerate(fd["entries"]):
            try:
                dico_article = {'titre':'',     
                                'date_publi': None,
                                'description':'',
                                'url_article':'',
                                'date_entree': None,
                                'id_source': id_db_source}
                dico_article['titre'] = fd.entries[i].title
                dico_article['url_article'] = fd.entries[i].link 
                if fd.entries[i].get('published_parsed'):
                    dico_article['date_publi'] = datetime.fromtimestamp(mktime(fd.entries[i].published_parsed)).date()
                else:
                    dico_article['date_publi'] = datetime.fromtimestamp(mktime(fd.entries[i].updated_parsed)).date()
                dico_article['date_entree'] = date.today()
                #['description'] = ...

                list_dicos_article.append(dico_article)
            except:
                pass
        return list_dicos_article


#exemples test
#url_trello = "http://tech.trello.com/"

# xmlUrl=["https://trello.engineering/feed.xml","http://tech.trivago.com/atom.xml" ,"http://engineering.gusto.com/rss/","http://engineering.quora.com/rss",
#         "http://engineering.tilt.com/atom.xml","https://blogs.dropbox.com/tech/feed/","http://tech.secretescapes.com/feed/",
#         "http://blog.developer.bazaarvoice.com/feed/" ,"http://blog.codeship.com/feed/","http://engineering.heroku.com/feed.xml" ,
#         "http://engineering.skybettingandgaming.com/feed.xml","http://engineering.indeedblog.com/feed/"]

# cp_dic=0
# cp_u=0
# id_s=0
# for url in xmlUrl:
#     cp_u+=1
#     print("\nurl:",cp_u)
#     dico = extract_source(url)
#     if dico:
#         id_s+=1
#         l_art = extract_articles(url,id_s)
#         cp_dic+=1
#         print("\nsource:",cp_dic)
#         for k in dico:
#             print(k,":",dico[k])
#     if l_art:
#         print("\nses articles:")
#         for d_a in l_art:
#             if type(d_a)==dict:
#                 for k in d_a:
#                     print(k,":",d_a[k])
#                 print("\n")    