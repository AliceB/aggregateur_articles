from flask import Flask, render_template, redirect, request, url_for, session
import DBQuery
import insert_delete as id
import connexion_db as cd
import extract_flux as ef
import password_hash as ph

app = Flask(__name__)

# clé de chiffrement pour la gestion des sessions
app.secret_key = b'\x97}\xcbN/\xde\xb9\xf8\xb4bs#\xacZ \xce'

"""
Ex :
if 'id_u' in session:
    <code pour afficher la page en fonction de session['id_u'] >
else:
    return redirect(url_for('se_connecter'))
"""

@app.route("/", methods=["GET"])
def get_consult():
    """
    Renvoie la page de consultation des articles si un utilisateur est identifié, 
    renvoie vers la page de connexion sinon.
    ____
    request.args['title_search'] : mot-clef pour filtrer les articles
    request.args['src[n]'] : source sélectionnée, avec n = id_s
    ____
    consult.html
        titre : titre à afficher en en-tête
        signature : sous-titre en en-tête
        nb_art : nb d'articles de la sélection courante
        all_art : liste de dictionnaires d'articles ('titre','url_article','date_publi','nm_source')
        title_search : idem entrée
        sources : liste des id_s sélectionnés

    """
    #session['id_u'] = 1
    #session['statut'] = "user"
    #session['log_u'] = "test"
    title_search = request.args.get("title_search", "")

    sour = {}
    sources = []

    if 'id_u' in session:
        for src, checked in request.args.items():
            if src[:3] == "src":
                sour[src[3:]] = 1
                sources.append(int(src[3:]))

        if session.get('statut','') != "admin":    
            if len(sources) == 0:
                srcs = DBQuery.get_user_sources(session['id_u'])
                for src in srcs:
                    sources.append(src['id_s'])
            
        articles = []
        nb_articles = 0

        all_art = DBQuery.get_all_articles(
                                    title_search,
                                    tuple(sources)
                                    )
        nb_art = len(all_art)

        articles.extend(all_art)
        nb_articles += nb_art
        
        if session.get('statut','') == "admin":        
            sources = DBQuery.get_all_sources()
        else:
            sources = DBQuery.get_user_sources(session['id_u'])

        for src in sources:
            if sour.get(str(src['id_s'])):
                src['checked'] =1
            else:
                src['checked'] =0
        return render_template(
            "consult.html",
            titre = """Aggregat0r v0.1""",
            signature = """Dominique, Alice, Kévin, Victor""",
            nb_art = nb_articles,
            all_art = articles,
            title_search = title_search,
            sources = sources,
            login = session.get("log_u",None),
            statut = session.get("statut",None)
        )

    else:
        # à décommenter pls tard
        
        return redirect(url_for('se_connecter'))
        
        for src, checked in request.args.items():
            if src[:3] == "src":
                sour[src[3:]] = 1
                sources.append(int(src[3:]))


        articles = []
        nb_articles = 0
    
        all_art = DBQuery.get_all_articles(
                                    title_search,
                                    tuple(sources)
                                    )
        nb_art = len(all_art)

        articles.extend(all_art)
        nb_articles += nb_art
   
        sources = DBQuery.get_all_sources()

        for src in sources:
            if sour.get(str(src['id_s'])):
                src['checked'] =1
            else:
                src['checked'] =0
        return render_template(
            "consult.html",
            titre = """Aggregat0r v0.1""",
            signature = """Dominique, Alice, Kévin, Victor""",
            nb_art = nb_articles,
            all_art = articles,
            title_search = title_search,
            sources = sources,
            login = session.get("log_u",None),
            statut = session.get("statut",None)

        )

@app.route("/new_source", methods=["GET"])
def get_new():
    if 'statut' in session and session['statut'] == 'admin':
        url_flux = request.args.get("url_flux", "")
        erreur_flux = 1
        with cd.conn_db() as conn:        
            #extraction du dico de la source
            dico = ef.extract_source(url_flux)
            if dico:
                #insertion data t_sources et recuperation de l'id_source
                id_s = id.insert_source(conn, dico)
                #extraction des articles de la source
                l_art = ef.extract_articles(url_flux,id_s)
                #insertion data t_articles 
                id.insert_article(conn, l_art)
                erreur_flux = 2
            elif url_flux:
                erreur_flux = 3

        sources = DBQuery.get_all_sources()

        return render_template(
            "new_source_admin.html",
            sources = sources,
            erreur_flux = erreur_flux,
            titre = """Gestion des sources""",
            signature = """Dominique, Alice, Kévin, Victor""",
            login = session.get("log_u",None),
            statut = session.get("statut",None)
        )
    
    elif 'statut' in session and session['statut'] == 'user':
        url_flux = request.args.get("url_flux", "")
        erreur_flux = 1
        id_u = session["id_u"]
        with cd.conn_db() as conn:        
            #test si la source demandée est déja dans la bdd:
            if DBQuery.get_ids(url_flux) is None:
                #nouvelle source: insertion de la source et de ses articles
                dico = ef.extract_source(url_flux)
                if dico:
                    id_s = id.insert_source(conn, dico)
                    l_art = ef.extract_articles(url_flux,id_s)
                    id.insert_article(conn, l_art)
                    erreur_flux = 2
                    #puis insertion de la source dans la table de liaison:
                    id.add_source_utilisateur(conn, id_u, id_s)
                
                elif url_flux:
                    erreur_flux = 3
            else:
                #source déja existante: insertion de la source dans la table de liaison seulement:
                id_s = DBQuery.get_ids(url_flux)
                id.add_source_utilisateur(conn, id_u, id_s)
                erreur_flux = 2
        sources_fav = DBQuery.get_user_sources(id_u)
        sources_dispo = DBQuery.get_sources_dispo(id_u)
        
        return render_template(
            "new_source_user.html",
            sources_fav = sources_fav,
            sources_dispo = sources_dispo,
            erreur_flux = erreur_flux,
            titre = """Gestion des sources""",
            signature = """Dominique, Alice, Kévin, Victor""",
            login = session.get("log_u",None),
            statut = session.get("statut",None)
            )
        
    else:
        return redirect(url_for('se_connecter'))


@app.route("/suppression", methods=["GET"])
def sup_source():
    test = request.args.items()
    # if 'id_u' in session:
    #     pass
    # else:
    if 'statut' in session and session['statut'] == 'admin':
        for src, a in request.args.items():
            if src[:3] == "src":
                a = int(src[3:])
                with cd.conn_db() as conn:
                    id.remove_source(conn, a)
        return redirect(url_for("get_new"))
    else:
        return redirect(url_for('get_consult'))


@app.route("/new_source_dispo_user", methods=["GET"])
def get_new_source_dispo_user():
    id_u = session['id_u']
    # id_u = 1
    for src, a in request.args.items():
        if src[:3] == "src":
            a = int(src[3:])
            with cd.conn_db() as conn:
                id.add_source_utilisateur(conn, id_u, a)
    return redirect(url_for("get_new"))
    

@app.route("/suppression_source_user", methods=["GET"])
def sup_source_user():
    id_u = session['id_u']
    # id_u = 1
    for src, a in request.args.items():
        if src[:3] == "src":
            a = int(src[3:])
            with cd.conn_db() as conn:
                id.remove_source_utilisateur(conn, id_u, a)
    return redirect(url_for("get_new"))


@app.route("/actualise_src")
def actualise_source():
    #prevoir le cas ou une source ou son flux n'existe pas !!!

    #recupere les url_flux et id_s des sources de la DataBase
    all_sources=DBQuery.get_all_sources()
    #pour chaque source(un dico)

    with cd.conn_db() as conn:        
        for source in all_sources:
            #extraire articles de source['url_flux'] -liste de dicos
            l_art = ef.extract_articles(source['url_flux'],source['id_s'])
            #insertion de l_art  dans t_articles 
            id.insert_article(conn, l_art)
                #actualiser les variables de la page
    return redirect(url_for('get_consult'))

@app.route("/connexion",methods=['GET','POST'])
def se_connecter():  
    error=None 
    if request.method=='POST':
        #a modifier pour recup email ou logu en fonction des infos entrées par le user
        logu=request.form['username']
        email=request.form['email']
        mdp=request.form['password']
        
        connexion_valide = DBQuery.get_user_mdp(logu,email)

        if connexion_valide:
            mdp_stored = DBQuery.get_user_info(logu, email)[2]
            connexion_valide = ph.verify_password(mdp_stored, request.form["password"])

        # user_mdp_exist = DBQuery.get_user_mdp(logu,email,mdp_h)
        if connexion_valide == False:
            session['email_u']=None
            error=" Cet utilisateur et/ou ce mot de passe ne sont pas valides, reessayer!"
            return render_template('connexion.html',
                                error=error,
                                titre = """Avec Aggregat0r, gérez vos sources d'infos favorites et restez au TOP de l'actu!!!""",
                                signature = """Dominique, Alice, Kévin, Victor""",
                                login = session.get("log_u",None),
                                statut = session.get("statut",None)
                                ) 
        elif connexion_valide == True:
            session['id_u'] = DBQuery.get_user_info(logu , email)[0]
            session['email_u'] = email
            session['log_u'] = logu
            session['statut'] = DBQuery.get_user_info(logu , email)[4]
            return redirect(url_for('get_consult'))

    elif request.method=='GET':
        return render_template('connexion.html',
                        error=error,
                        titre = """Avec Aggregat0r, gérez vos sources d'infos favorites et restez au TOP de l'actu!!!""",
                        signature = """Dominique, Alice, Kévin, Victor""",
                        login = session.get("log_u",None),
                        statut = session.get("statut",None)
                        )     

@app.route("/inscription",methods=['GET','POST'])
def s_inscrire():
    error=None

    if 'id_u' in session:
        return redirect(url_for('get_consult'))

    if request.method == 'GET' :
        return render_template('inscription.html',
                            titre = """Avec Aggregat0r, gérez vos sources d'infos favorites et restez au TOP de l'actu!!!""",
                            signature = """Dominique, Alice, Kévin, Victor""",
    
                            )

    if request.method == 'POST' :
        dict_utilisateur={}
        dict_utilisateur['log_u'] = request.form["username"]
        dict_utilisateur['email_u'] = request.form["email"]
        dict_utilisateur['statut_u'] = 'user'
        # nom et mail existent deja
        user_exist = DBQuery.get_user(dict_utilisateur['log_u'], dict_utilisateur['email_u'])
        if user_exist:
            error = " utilisateur déjà enregistré avec ce pseudo ou cette adresse mail "
            return render_template('inscription.html',
                                    error=error,
                                    titre = """Avec Aggregat0r, gérez vos sources d'infos favorites et restez au TOP de l'actu!!!""",
                                    signature = """Dominique, Alice, Kévin, Victor""",
                                    )

        else:
            if request.form["password"] == request.form["password_bis"]:
                #hachage du mdp
                dict_utilisateur['mdp_u'] = ph.hash_password(request.form["password"])
                #insertion data dans table t_utilisateur
                print(dict_utilisateur)
                with cd.conn_db() as conn:   
                    if dict_utilisateur:     
                        id.insert_utilisateur(conn, dict_utilisateur)
                    return render_template('connexion.html',
                            titre = """Avec Aggregat0r, gérez vos sources d'infos favorites et restez au TOP de l'actu!!!""",
                            signature = """Dominique, Alice, Kévin, Victor""",
                            login = session.get("log_u",None),
                            statut = session.get("statut",None)
                            )    
            else:
                error= '... Mot de passe non confirmé!  Recommencez !'
                return render_template('inscription.html', 
                        error=error,
                        titre = """Avec Aggregat0r, gérez vos sources d'infos favorites et restez au TOP de l'actu!!!""",
                        signature = """Dominique, Alice, Kévin, Victor""",
                        login = session.get("log_u",None),
                        statut = session.get("statut",None)
                        )    
               
@app.route("/deconnexion")
def se_deconnecter():
    session.pop("id_u",None)
    session.pop("log_u",None)
    session.pop("statut",None)
    return redirect(url_for('se_connecter'))
